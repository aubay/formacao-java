package com.aubay.class1.funcionalinterface;

public class StudentMain {

    public static void main(String... args) {
        new StudentMain().lambda();
    }

    private void lambda() {
        StudentInterface sInterface = (message) -> {
            System.out.println("Hello " + message);
        };

        sInterface.sayMessage("Fabio");
        sInterface.sayMessage("Gustavo");
        sInterface.sayMessage("M�rcio");
        sInterface.sayMessage("Marlene");
        sInterface.sayMessage("Diogo");


        sayMessage("Fabio", new Hello());
        sayMessage("Gustavo", new Hello());
    }

    public void sayMessage(String message, StudentInterface sInterface) {
        sInterface.sayMessage(message);
    }


    @FunctionalInterface
    interface StudentInterface {
        void sayMessage(String message);
    }

    class Hello implements StudentInterface {
        @Override
        public void sayMessage(String message) {
            System.out.println("Hello " + message);
        }
    }


}
