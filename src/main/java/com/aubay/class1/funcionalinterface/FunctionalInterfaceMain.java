package com.aubay.class1.funcionalinterface;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;

public class FunctionalInterfaceMain {

    public static void main(String... args) {
        new FunctionalInterfaceMain().main();
    }

    private void main() {
        Double n1 = 10d;
        Double n2 = 2d;

        // TODO call the calc method for 4 operations
        calc(n1, n2, ((v1, v2) -> v1 + v2));
        calc(n1, n2, ((v1, v2) -> v1 - v2));
        calc(n1, n2, ((v1, v2) -> v1 * v2));
        calc(n1, n2, ((v1, v2) -> v1 / v2));

        List<String> list = new ArrayList<>();
        list.add("String");

    }

    private void calc(Double n1, Double n2, BiFunction<Double, Double, Double> op) {
        System.out.println(op.apply(n1, n2));
    }

}
