package com.aubay.class1.funcionalinterface;

import java.math.BigDecimal;

public class OldSchool {

    public static void main(String... args) {
        new OldSchool().main();
    }

    private void main() {
        BigDecimal n1 = BigDecimal.valueOf(10);
        BigDecimal n2 = BigDecimal.valueOf(2);

        print(n1, n2, new Add());
        print(n1, n2, new Subtract());
        print(n1, n2, new Multiply());
        print(n1, n2, new Divide());
    }

    private void print(BigDecimal n1, BigDecimal n2, Math m) {
        System.out.println("Calling operator " + m.getClass().getSimpleName());
        System.out.println(m.math(n1, n2));
    }

    //--------- Interface and implementations
    private interface Math {
        BigDecimal math(BigDecimal n1, BigDecimal n2);
    }

    private class Add implements Math {
        @Override
        public BigDecimal math(BigDecimal n1, BigDecimal n2) {
            return n1.add(n2);
        }
    }

    private class Subtract implements Math {
        @Override
        public BigDecimal math(BigDecimal n1, BigDecimal n2) {
            return n1.subtract(n2);
        }
    }

    private class Multiply implements Math {
        @Override
        public BigDecimal math(BigDecimal n1, BigDecimal n2) {
            return n1.multiply(n2);
        }
    }

    private class Divide implements Math {
        @Override
        public BigDecimal math(BigDecimal n1, BigDecimal n2) {
            return n1.divide(n2, 0);
        }
    }

}

