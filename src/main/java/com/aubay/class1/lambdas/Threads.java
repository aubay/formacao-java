package com.aubay.class1.lambdas;

public class Threads {

    public static void main(String... args) {
        new Thread(new Runnable() {

            @Override
            public void run() {
                System.out.println("Running a new thread...");
            }

        }).start();

    }
}
