package com.aubay.class1.lambdas;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class LambdasMain {

    public static void main(String... args) {
        List<String> alunos = new LinkedList<>();
        alunos.add("Diogo Pimentel");
        alunos.add("Marlene Borges");
        alunos.add("Alexandre Brito");
        alunos.add("Marcio Sansone");
        alunos.add("Gustavo Monteiro");
        alunos.add("Fabricio Reis");

        Consumer<String> stringConsumer = a -> System.out.println(a);

        Predicate<String> predicate = aluno -> {
           if (aluno.length() > 5) {
               return true;
           }
           return false;
        };


        predicate.test("Fabio");
        predicate.test("Gustavo");

    }

}
