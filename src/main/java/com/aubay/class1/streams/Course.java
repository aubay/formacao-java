package com.aubay.class1.streams;

public class Course {
    private String name;
    private int students;
    private CourseType type;

    public Course() {
    }

    public Course(String name, int students, CourseType type) {
        this.name = name;
        this.students = students;
        this.type = type;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setStudents(int students) {
        this.students = students;
    }

    public void setType(CourseType type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public int getStudents() {
        return students;
    }

    public CourseType getType() {
        return type;
    }

    @Override
    public String toString() {
        return "Course{" +
                "name='" + name + '\'' +
                ", students=" + students +
                ", type=" + type +
                '}';
    }
}