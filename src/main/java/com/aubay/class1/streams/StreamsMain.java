package com.aubay.class1.streams;

import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toMap;

public class StreamsMain {

    public static void main(String... args) {

        List<Course> courses = new ArrayList<>();
        courses.add(new Course("Java", 6, CourseType.BACKEND));
        courses.add(new Course("Javascript", 8, CourseType.FRONTEND));
        courses.add(new Course("Python", 1, CourseType.BACKEND));
        courses.add(new Course("Node.js", 10, CourseType.BACKEND));
        courses.add(new Course("React", 15, CourseType.FRONTEND));

        courses.stream()
                .collect(toMap(Course::getType, Course::getStudents, (o1, o2) -> o1 + o2))
                .forEach((k, v) -> {
                    System.out.println("Tipo: " + k);
                    System.out.println("Total: " + v);
                });


    }

    private static void save(Course c) {


    }

}
