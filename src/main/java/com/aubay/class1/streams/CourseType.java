package com.aubay.class1.streams;

public enum CourseType {
    BACKEND, FRONTEND, DATABASE
}
