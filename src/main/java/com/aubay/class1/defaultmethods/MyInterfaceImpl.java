package com.aubay.class1.defaultmethods;

public class MyInterfaceImpl implements MyInterface {

    public static void main(String... args) {
        new MyInterfaceImpl().canIPrint();
    }

    @Override
    public void print() {

    }

}

interface MyInterface {
    
    void print();

    default void canIPrint() {
        System.out.println("Test");
    }

}

abstract class MyInterfaceAbstract {

    private String name;

    abstract void print();

    public void canIPrint() {
        System.out.println("Test");
    }

}

