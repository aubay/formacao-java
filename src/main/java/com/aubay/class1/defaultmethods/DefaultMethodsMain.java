package com.aubay.class1.defaultmethods;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class DefaultMethodsMain {

    public static void main(String... args) {
        sort();
    }

    private static void sort() {
        List<String> alunos = new LinkedList<>();
        alunos.add("Diogo Pimentel");
        alunos.add("Marlene Borges");
        alunos.add("Alexandre Brito");
        alunos.add("Marcio Sansone");
        alunos.add("Gustavo Monteiro");
        alunos.add("Fabricio Reis");

        alunos.sort(new ComparatorBySize());

        Collections.sort(alunos);
        System.out.println(alunos);
    }
}
