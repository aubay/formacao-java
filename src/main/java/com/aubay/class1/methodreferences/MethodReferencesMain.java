package com.aubay.class1.methodreferences;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class MethodReferencesMain {
    public static void main(String... args) {
        List<String> alunos = new LinkedList<>();
        alunos.add("Diogo Pimentel");
        alunos.add("Marlene Borges");
        alunos.add("Alexandre Brito");
        alunos.add("Marcio Sansone");
        alunos.add("Gustavo Monteiro");
        alunos.add("Fabricio Reis");

        alunos.sort(Comparator.comparing(s -> s.length()));
    }
}
